===========================================
Electric Vehicle Charging Station Protocols
===========================================

OCPP - Open Charge Point Protocol
=================================

`The Open Charge Allience <https://www.openchargealliance.org/>`_

Create a virtual environment.

.. code-block:: console

		$ pyenv install 3.7.0
		Downloading Python-3.7.0.tar.xz...
		-> https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz
		Installing Python-3.7.0...
		Installed Python-3.7.0 to ~/.pyenv/versions/3.7.0

.. code-block:: console

		$ pyenv virtualenv 3.7.0 ocpp_client
		Looking in links: /tmp/tmpwi_ix0mh
		Requirement already satisfied: setuptools in ~/.pyenv/versions/3.7.0/envs/ocpp_client/lib/python3.7/site-packages (39.0.1)
		Requirement already satisfied: pip in ~/.pyenv/versions/3.7.0/envs/ocpp_client/lib/python3.7/site-packages (10.0.1)

Activate the virtual environment

.. code-block:: console

		$ pyenv activate ocpp_client
		(ocpp_client) $ python --version
		Python 3.7.0

Install the package.

.. code-block:: console

		(ocpp_client) $ pip install ioe
		Collecting ioe
                  Downloading https://files.pythonhosted.org/packages/c9/b6/1559662acc8373d37ca2cd4cfdf26b097f29c5ed62fa825dd87866750990/ioe-0.0.26-py3-none-any.whl (85kB)
                    100% |████████████████████████████████| 92kB 7.3MB/s
                Collecting websockets (from ioe)
                  Downloading https://files.pythonhosted.org/packages/aa/53/1dbfbe51e8ba9a2b9bc0b7201df77fe597f1e57b6b5d9bb094d3729aeecf/websockets-7.0-cp37-cp37m-manylinux1_x86_64.whl (63kB)
                    100% |████████████████████████████████| 71kB 10.9MB/s
                Collecting websocket-client (from ioe)
                  Downloading https://files.pythonhosted.org/packages/26/2d/f749a5c82f6192d77ed061a38e02001afcba55fe8477336d26a950ab17ce/websocket_client-0.54.0-py2.py3-none-any.whl (200kB)
                    100% |████████████████████████████████| 204kB 10.7MB/s
                Collecting jsonschema (from ioe)
                  Downloading https://files.pythonhosted.org/packages/77/de/47e35a97b2b05c2fadbec67d44cfcdcd09b8086951b331d82de90d2912da/jsonschema-2.6.0-py2.py3-none-any.whl
                Collecting six (from websocket-client->ioe)
                  Downloading https://files.pythonhosted.org/packages/67/4b/141a581104b1f6397bfa78ac9d43d8ad29a7ca43ea90a2d863fe3056e86a/six-1.11.0-py2.py3-none-any.whl
                Installing collected packages: websockets, six, websocket-client, jsonschema, ioe
                Successfully installed ioe-0.0.26 jsonschema-2.6.0 six-1.11.0 websocket-client-0.54.0 websockets-7.0

OCPP Client
-----------

Run server
~~~~~~~~~~

.. code-block:: console

	(ocpp_client) $ python -m ioe.ocpp.server
	Serving OCPP on 0.0.0.0 port 8765 (ws://0.0.0.0:8765/) ...


Send a BootNotification request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

	>>> import ioe.ocpp.client as ocpp
	>>> cs = ocpp.OCPPConnection('ws://localhost:8765/')
	>>> cs.request('BootNotification', {'reason': 'PowerUp','chargingStation': {'model': '1','vendorName': 'a'}})
	{'url': 'ws://localhost:8765/', 'data': '{"reason": "PowerUp", "chargingStation": {"model": "1", "vendorName": "a"}}'}


When something is wrong with message, errors are notified
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For exmple, missing parameters in the boot notification request.

.. code-block:: python

	>>> cs.request('BootNotification', {'reason': 'PowerUp','chargingStation': {'model': '1'}})
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	  File "/home/sergio/.pyenv/versions/ocpp_client/lib/python3.7/site-packages/ioe/ocpp/client.py", line 38, in request
	    validate(payload, self.schemas_dict[message + 'Request_v1p0.json'])
	  File "/home/sergio/.pyenv/versions/ocpp_client/lib/python3.7/site-packages/jsonschema/validators.py", line 541, in validate
	    cls(schema, *args, **kwargs).validate(instance)
	  File "/home/sergio/.pyenv/versions/ocpp_client/lib/python3.7/site-packages/jsonschema/validators.py", line 130, in validate
	    raise error
	jsonschema.exceptions.ValidationError: 'vendorName' is a required property

	Failed validating 'required' in schema['properties']['chargingStation']:
	    {'additionalProperties': True,
	     'javaType': 'ChargingStation',
	     'properties': {'firmwareVersion': {'maxLength': 50, 'type': 'string'},
	                    'model': {'maxLength': 20, 'type': 'string'},
	                    'modem': {'$ref': '#/definitions/ModemType'},
	                    'serialNumber': {'maxLength': 20, 'type': 'string'},
	                    'vendorName': {'maxLength': 50, 'type': 'string'}},
	     'required': ['model', 'vendorName'],
	     'type': 'object'}

	On instance['chargingStation']:
	    {'model': '1'}
